pyphen (0.16.0-1) unstable; urgency=medium

  * New upstream release
    - Refresh patches
  * Remove extra pybuild-plugin-pyproject from Build-Depends in d/control

 -- Scott Kitterman <scott@kitterman.com>  Fri, 02 Aug 2024 12:27:17 -0400

pyphen (0.15.0-1) unstable; urgency=medium

  * New upstream release
  * Update and rebase patches
  * Bump standards-version to 4.7.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 04 May 2024 16:04:25 -0400

pyphen (0.14.0-1) unstable; urgency=medium

  * New upstream release
  * Update and rebase patches
  * Bump standards-version to 4.6.2 without further change
  * Switch from deprecated flit plugin to pybuild-plugin-pyproject

 -- Scott Kitterman <scott@kitterman.com>  Mon, 12 Jun 2023 16:30:44 -0400

pyphen (0.13.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 03 Dec 2022 14:31:03 -0500

pyphen (0.13.1-2) unstable; urgency=medium

  * Team upload.
  * d/control: Use the generic pyproject pybuild plugin.
  * d/control: Bump DebHelper compat level to 13.
  * d/control: Add Rules-Requires-Root statement.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Fri, 02 Dec 2022 12:08:50 -0500

pyphen (0.13.1-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Mon, 21 Nov 2022 18:50:14 -0500

pyphen (0.13.0-1) unstable; urgency=medium

  * New upstream release
    - Refresh patches
  * Update homepage to page referenced from PyPI (pyphen.org not updated)
  * Bump standards-version to 4.6.1 without further change
  * Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Mon, 17 Oct 2022 11:15:09 -0400

pyphen (0.12.0-1) unstable; urgency=medium

  * New upstream release
    - Refreshed patches
  * Add chmod to d/rules to make pyphen/__init__.py not executable
  * Add Origin and Forwarded fields to patch headers
  * Update debian/watch to version 4

 -- Scott Kitterman <scott@kitterman.com>  Wed, 29 Dec 2021 15:06:34 -0500

pyphen (0.11.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.

  [ Scott Kitterman ]
  * Update debian/watch to work with lower case project name
  * New upstream release
    - Drop all patches, need substantial rework
    - Remove obsolete dh auto test override
    - Update debian/python3-pyphen.docs for new README file name
  * Add flit and drop setuptools from build-depends to use flit pybuild plugin
  * Add debian/patches/0001-remove-flake8-isort-from-tests.patch to eliminate
    irrelevant tests
  * Remove pyhen dictionaries from built package since they are not used
  * Add debian/patches/0002-Use-system-hyphen-dictionaries.patch so Debian
    system hyphenation dictionaries are still used
  * Add debian/patches/0003-Update-test_fallback-test-for-Debian-naming.patch
    to match Debian hyphenation packages
  * Add additional hyphenation dictionaries to build-depends and test depends
    to support new tests
  * Bump standards-version to 4.6.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Wed, 01 Dec 2021 13:42:22 -0500

pyphen (0.9.5-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.

 -- Ondřej Nový <onovy@debian.org>  Thu, 01 Aug 2019 12:37:29 +0200

pyphen (0.9.5-2) unstable; urgency=medium

  * Drop lintian-override for new-package-should-not-package-python2-module as
    requested by lamby
  * Python 2 version needed to support potential backport to stretch

 -- Scott Kitterman <scott@kitterman.com>  Sat, 05 Jan 2019 03:16:42 -0500

pyphen (0.9.5-1) unstable; urgency=low

  * Initial Debian package (Closes: #917039)
  * Add d/p/0001-Use-system-hyphen-dictionaries.patch so that duplicate hyphen
    dictionaries are not shipped in the binaries and system dictionaries are
    used
  * Enable build and autopkgtests
    - Add d/p/0002-Add-tests-from-github.patch so that upstream tests are
      available
    - Add d/p/0003-Disable-tests-needing-missing-fallback-data.patch so that
      tests the need data not currently available from Debian hyphen
      dictionaries are not run
    - Autopkgtest runs enabled upstream tests for all supported python and
      python3 versions

 -- Scott Kitterman <scott@kitterman.com>  Fri, 04 Jan 2019 18:27:12 -0500
